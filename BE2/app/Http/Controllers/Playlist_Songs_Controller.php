<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Playlist_Songs;

class Playlist_Songs_Controller extends Controller
{
    public function disp_PlaylistSongs(){
        return DB::table('playlist__songs')->get();
    }

    public function add(Request $request){

        $newPlaylist__Songs = new Add();
        $newPlaylist__Songs->song_id = $request->song_id;
        $newPlaylist__Songs->playlist_id = $request->playlist_id;
        $newPlaylist__Songs->save();
        return $newPlaylist__Songs;

    }
}
