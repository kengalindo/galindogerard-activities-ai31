<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Playlists;

class Playlists_Controller extends Controller
{
    public function disp_PlaylistSongs(){
        return DB::table('playlists')->get();
    }

    public function create(Request $request){

        $newPlaylist = new Playlist();
        $newPlaylist->name = $request->name;
        $newPlaylist->save();
        return $newPlaylist;

    }
}
