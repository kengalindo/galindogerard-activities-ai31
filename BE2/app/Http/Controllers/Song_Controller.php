<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Songs;

class Song_Controller extends Controller
{
    public function disp_Songs(){
        return DB::table('songs')->get();
    }
    
    public function upload(Request $request){

        $newSongs = new Songs();
        $newSongs->title = $request->title;
        $newSongs->length = $request->length;
        $newSongs->artist = $request->artist;
        $newSongs->save();
        return $newSongs;

    }
}
