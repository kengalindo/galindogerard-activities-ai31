<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Song_Controller;
use App\Http\Controllers\Playlists_Controller;
use App\Http\Controllers\Playlist_Songs_Controller;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//Upload
Route::get('/songs', [Song_Controller::class, 'disp_Songs']);
Route::post('/upload', [Song_Controller::class, 'upload']);

//Create
Route::get('/playlist', [Playlists_Controller::class, 'disp_Playlists']);
Route::post('/create', [Playlists_Controller::class, 'create']);

//List
Route::get('/playlistsongs', [Playlist_Songs_Controller::class, 'disp_PlaylistSongs']);
Route::post('/add', [Playlist_Songs_Controller::class, 'add']);
