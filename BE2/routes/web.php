<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Song_Controller;
use App\Http\Controllers\Playlists_Controller;
use App\Http\Controllers\Playlist_Songs_Controller;
use Illuminate\Support\Facades\DB;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Upload
Route::get('/songs', [Song_Controller::class, 'disp_Songs']);
Route::post('/upload', [Song_Controller::class, 'upload']);

//Create
Route::get('/playlist', [Playlists_Controller::class, 'disp_Playlists']);
Route::post('/create', [Playlists_Controller::class, 'create']);

//List
Route::get('/playlistsongs', [Playlist_Songs_Controller::class, 'disp_PlaylistSongs']);
Route::post('/add', [Playlist_Songs_Controller::class, 'add']);

